#include "server.h"

struct Server dataServer;

void CreateServer( ){

    struct sockaddr_in serverAddress;
    struct sockaddr_in clientAddress;

    int server, clientAddressSize;

    printlnStatusSuccess("Thread Created");

    definePORT();
    defineServerConfiguration( &serverAddress, &clientAddress, &clientAddressSize );

    createServerSocket( &server );
    bindServer( server, serverAddress );
    listenClients( server );

    printlnLog("Configuration and Creation Server Finished");

    acceptClients( server, clientAddress, clientAddressSize );

}

void * ThreadCliente( void * arguments ){

    printlnStatusSuccess("Thread Created");

    struct Identificador * client = (struct Identificador *) arguments;
    struct Clients * _client;

    char clientBuffer[BUFFER_SIZE];
    char NAME[BUFFER_SIZE];

    setbuf(stdin, NULL);
    read(client->clienteFd, NAME, BUFFER_SIZE);

    _client = newClient( client->clienteFd, NAME );
    insertClientInList( client->listaClientes , _client );

    printlnStatusSuccess("Client add in List");
    showAllClients( client->listaClientes );
//-----------------------------------------------------------------------------
    while(TRUE) {

        setbuf(stdin, NULL);
        size_t  size = read(client->clienteFd, clientBuffer, BUFFER_SIZE);
        clientBuffer[size + 1] = '\0';

        printf(" ++++ @%s :: %s", NAME, clientBuffer);

        sendMessageAllClients(NAME, client->listaClientes, clientBuffer);

    }
//-----------------------------------------------------------------------------
    return(NULL);

}

void definePORT(){

    int PORT;

    printInput("Insert Server PORT");
    scanf("%d", &PORT);

    dataServer.PORT = PORT;

}

void defineServerConfiguration( struct sockaddr_in * serverAddress, struct sockaddr_in * clientAddress, int * clientAddressSize ){

    serverAddress->sin_family = AF_INET;
    serverAddress->sin_port = htons( dataServer.PORT );
    serverAddress->sin_addr.s_addr = INADDR_ANY;

    * clientAddressSize = sizeof( clientAddress );

    printlnStatusSuccess("Server Configurated");

}

void createServerSocket( int * server ){

    * server = socket( AF_INET, SOCK_STREAM, ZERO );

    if( server < ZERO )
        error("Opening socket");
    else
        printlnStatusSuccess("Socket Opened");

}

void bindServer( int server, struct sockaddr_in serverAddress ){

    if( bind(server, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < ZERO )
        error("On binding");
    else
        printlnStatusSuccess("Server Binding");

}

void listenClients( int server ){

    if( listen(server, ONE) < ZERO )
        error("On listening");
    else
        printlnStatusSuccess("Server Listening");

}

void acceptClients( int server, struct sockaddr_in clientAddress, int clientAddressSize ){

    int client;

    pthread_t thread;

    struct List clientsList;
    struct Identificador identificador;

    createClientsList( &clientsList );

    while( TRUE ){

        printlnLog("Waiting Clients");
        printf("\n");

        if( ( client = accept(server, (struct sockaddr *)&clientAddress, &clientAddressSize) ) < ZERO )
            error("On accept");
        else{
            printf("\n------------------------------------------------------------------\n\n");
            printlnStatusSuccess("Client Accepted");

            identificador.clienteFd = client;
            identificador.listaClientes = &clientsList;

            if( pthread_create(&thread, NULL, ThreadCliente , (void *) &identificador ) != ZERO )
                error("On Thread");

            pthread_detach( thread );

        }

    }

}