#include "server_task.h"
#include "../Input_Output/output.h"
#include <string.h>

void sendPrivateMessage( int client, char clientBuffer[] );

void sendMessageAllClients( char NAME[], struct List * clientsList, char MSG[]){

    struct Clients * cell;

    size_t t_nome = strlen( NAME );
    size_t t_msg = strlen( MSG );
    size_t total = t_msg + t_nome + 7;

    char formatMSG[total];

    if( clientsList->root != NULL ){

        cell = clientsList->root;

        while( cell != NULL ){

            if( strcmp(cell->name, NAME) != ZERO ){

                strcpy(formatMSG, NAME);
                strcat(formatMSG, " >> \n\n");
                strcat(formatMSG, MSG);
                strcat(formatMSG, "\n");

                printlnLog( formatMSG );

                write(cell->id, formatMSG, total );

            }

            cell = cell->next;

        }

        printlnStatusSuccess("Mensagem Enviada");
        printf("\n");

    }

}

void * ReceiveClient( void * arguments ){

    struct Clients * client = (struct Clients *) arguments;

    char clientBuffer[BUFFER_SIZE];

    setbuf(stdin, NULL);
    read(client->id, clientBuffer, BUFFER_SIZE);

    printf(" ++++ @%s :: %s\n", client->name, clientBuffer );

}


