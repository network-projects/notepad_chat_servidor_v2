#ifndef SERVER_TASK_H_INCLUDED
#define SERVER_TASK_H_INCLUDED

#include "../types.h"
#include "../ClientsList/clientsList.h"

#include "../Input_Output/input.h"
#include "../Input_Output/output.h"
#include "../ClientsList/clientsList.h"
#include "../ServerTask/server_task.h"
#include "../Controller/controller.h"

void sendPrivateMessage( int client, char clientBuffer[] );

void sendMessageAllClients( char NAME[], struct List *, char MSG[] );

void * ReceiveClient( void * arguments );

#endif