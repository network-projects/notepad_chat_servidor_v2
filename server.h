#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED

#include "types.h"

#include "Input_Output/input.h"
#include "Input_Output/output.h"
#include "ClientsList/clientsList.h"
#include "ServerTask/server_task.h"
#include "Controller/controller.h"


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

struct Server{

    int PORT;

};

struct Identificador{

    int clienteFd;

    struct List * listaClientes;

};

void CreateServer( );
void * ThreadCliente( void * arguments );

void definePORT();

void defineServerConfiguration( struct sockaddr_in *, struct sockaddr_in *, int * );
void createServerSocket( int * );
void bindServer( int, struct sockaddr_in );
void listenClients( int );
void acceptClients( int, struct sockaddr_in, int );

#endif
