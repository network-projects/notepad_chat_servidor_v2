#ifndef CLIENTS_LIST_H_INCLUDED
#define CLIENTS_LIST_H_INCLUDED

#include "../types.h"

struct Clients{

    int id;

    char name[BUFFER_SIZE];

    struct Clients * next;

};

struct List{

    struct Clients * root;

    pthread_mutex_t lock;

};

void createClientsList( struct List * );
void destruirLista( struct List * );
struct Clients * newClient( int, char * );

enum boolean checkExistingClientName( struct List *, char name[] );

void insertClientInList( struct List *, struct Clients * );
void removeClientInList( struct List *, char name[] );

void showClient( struct Clients * );
void showAllClients( struct List * );

#endif
