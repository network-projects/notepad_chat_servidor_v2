#include "clientsList.h"

void createClientsList( struct List * clientsList ){

    clientsList->root = NULL;

    pthread_mutex_init( &clientsList->lock, NULL );

}

void destruirLista( struct List * list ){

    pthread_mutex_destroy(&list->lock);

}

struct Clients * newClient( int id , char name[] ){

    struct Clients * _new;

    _new = ( struct Clients * )malloc( 1 * sizeof( struct Clients ) );

    _new->id = id;

    strcpy( _new->name, name );

    _new->next = NULL;

    return _new;

}

enum boolean checkExistingClientName( struct List * list, char name[] ){

    struct Clients * cell = list->root;

    enum boolean check = FALSE;

    if( list->root != NULL ){

        do{

            if( strcmp( cell->name, name ) == ZERO )
                check = TRUE;

            cell = cell->next;

        }while( cell != NULL );

    }

    return check;

}

void insertClientInList( struct List * list, struct Clients * client ){

    pthread_mutex_lock(&list->lock);

    struct Clients * cell = list->root;

    if( list->root == NULL ){

        list->root = client;

    }else{

        while( cell->next != NULL ){

            cell = cell->next;

        }

        cell->next = client;

    }

    pthread_mutex_unlock(&list->lock);

}

void removeClientInList( struct List * list, char name[] ){

    struct Clients * cell = list->root;
    struct Clients * previous;

    if( list->root != NULL ){

        while( (cell != NULL) && (strcmp(cell->name, name) != ZERO) ){

            previous = cell;

            cell = cell->next;

        }

        if( strcmp(cell->name, name) == ZERO ){

            previous = cell->next;

            free( cell );

        }

    }

}

void showClient( struct Clients * client ){

    fprintf(stdout, "\033[01;33m# ~ >> ID(%d) :: NAME(%s) << ~ #\n", client->id, client->name);

}

void showAllClients( struct List * list ){

    struct Clients * cell = list->root;

    if( list->root != NULL ){

        while( cell != NULL ){

            fprintf(stdout, "\033[01;33m ****** # ~ >> ID(%d) :: NAME(%s) << ~ #\n", cell->id, cell->name);

            cell = cell->next;

        }

    }

    printf("\n");

}
