#include "output.h"

void printlnLog( char * string ){

    fprintf(stdout, "\033[01;33m# ~ >> Log : < char * > : %s << ~ #\n", string);

}

void printlnLogInt( int number ){

    fprintf(stdout, "\033[01;33m# ~ >> Log : < int >  : %d << ~ #\n", number);

}

void printlnStatusSuccess( char * string ){

    fprintf(stdout, "\033[01;32m# ~ >> %s :: Success << ~ #\n", string);

}

void printlnStatusError( char * string ){

    fprintf(stdout, "\033[01;31m# ~ >> %s :: Error << ~ #\n", string);

}

void printInput( char * string ){

    fprintf(stdout, "\033[01;37m# ~ >> %s >> ", string);

}

void printMsg( char * string ){

    fprintf(stdout, "\033[01;36m# ~ >> %s\n", string);

}

void error( const char * string ){

    enum numbers status = ONE;

    fprintf(stderr, "\033[01;31m# ~ >> ERROR :: %s << ~ #\n\n", string);

    exit( status );

}