#include "input.h"

void getString( char string[] ){

    char character;
    int count = 0;

    do{

        character = getchar_unlocked();

        string[ count ] = character;

        count++;

    }while( character != '\n' );

    string[ count - 1 ] = '\0';

}

void getUnlockString( char string[] ){

    fd_set readfds;
	int num_readable;
	struct timeval tv;
	int num_bytes;
	int fd_stdin;

	fd_stdin = fileno(stdin);

	while( TRUE ){

		FD_ZERO(&readfds);
		FD_SET(fileno(stdin), &readfds);

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		fflush(stdout);

		num_readable = select(fd_stdin + 1, &readfds, NULL, NULL, &tv);
		if(num_readable == -1){

			fprintf(stderr, "\nERROR\n\n", strerror(errno));
			exit(1);

		}

		if(num_readable == 0){

            //after 1 sec
			break;

		}else{

			num_bytes = read(fd_stdin, string, BUFFER_SIZE);
			if(num_bytes < 0){
				exit(1);

			}
            break;

		}

	}

}