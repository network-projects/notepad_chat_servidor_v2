#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include "../types.h"

void getString( char string[] );
void getUnlockString( char string[] );

#endif