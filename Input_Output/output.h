#ifndef OUTPUT_H_INCLUDED
#define OUTPUT_H_INCLUDED

#include "../types.h"

void printlnLog( char * );
void printlnLogInt( int number );

void printlnStatusSuccess( char * );
void printlnStatusError( char * );

void printInput( char * );
void printMsg( char * );

void error( const char * );

#endif