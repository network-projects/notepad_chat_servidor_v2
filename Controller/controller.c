#include "controller.h"

void defineNameProtocol( char protocol[], char name[], enum boolean existName ){

    strcpy( protocol, name );

    if( existName == FALSE ){

        strcat( protocol, "::NON_EXIST");

    }else{

        strcat( protocol, "::EXIST");

    }

    printlnStatusSuccess("Protocol Generated");
    printlnLog(protocol);

}