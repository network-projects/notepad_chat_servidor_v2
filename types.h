#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <pthread.h>

#define BUFFER_SIZE 4096

enum boolean{ FALSE, TRUE };
enum numbers{ LESS_ONE = -1, ZERO, ONE };

#endif